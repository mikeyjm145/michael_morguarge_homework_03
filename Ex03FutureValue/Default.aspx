﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Morguarge HW03 Future Value</title>
    <link href="murachStyles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <img id="logo" alt="Murach logo" src="Images/MurachLogo.jpg"/>
    </header>
    <section>
        <form id="form1" runat="server">
            
            <h1>
                401K Future Value Calculator
            </h1>

            <br />

            <label>
                Monthly investment:
            </label>

            <asp:DropDownList
                ID="ddlMonthlyInvestment"
                CssClass="entry"
                runat="server"></asp:DropDownList>

            <br />

            <label>
                Annual interest rate:
            </label>

            <asp:TextBox
                ID="txtInterestRate"
                CssClass="entry"
                runat="server">6.0</asp:TextBox>
            
            <asp:RequiredFieldValidator
                ID="InterestRateFieldValidator"
                CssClass="validator"
                runat="server" 
                ErrorMessage="Interest rate is required." 
                ControlToValidate="txtInterestRate"
                Display="Dynamic"
                ForeColor="Red">
            </asp:RequiredFieldValidator>
        
            &nbsp;

            <asp:RangeValidator
                ID="InterestRateRangeValidator"
                CssClass="validator"
                runat="server" 
                ControlToValidate="txtInterestRate"
                Display="Dynamic" 
                ErrorMessage="Interest rate must range from 1 to 20."
                MaximumValue="20" 
                MinimumValue="1"
                Type="Double"
                ForeColor="Red">
            </asp:RangeValidator>
            
            <br />

            <label>
                Number of years:
            </label>

            <asp:TextBox ID="txtYears"
                CssClass="entry"
                runat="server">10</asp:TextBox>
            
            <asp:RequiredFieldValidator
                ID="NumberOfYearsFieldValidator"
                CssClass="validator"
                runat="server"
                ControlToValidate="txtYears"
                Display="Dynamic"
                ErrorMessage="Number of years is required."
                ForeColor="Red">
            </asp:RequiredFieldValidator>
        
            &nbsp;

            <asp:RangeValidator
                ID="NumberOfYearsRangeValidator"
                CssClass="validator"
                runat="server"
                ControlToValidate="txtYears"
                Display="Dynamic"
                ErrorMessage="Years must range from 1 to 45."
                MaximumValue="45"
                MinimumValue="1"
                Type="Integer"
                ForeColor="Red">
            </asp:RangeValidator>

            <br />
            
            <label>
                Future value:
            </label>

            <asp:Label ID="lblFutureValue"
                CssClass="entry"
                runat="server"
                Text=""></asp:Label>
            
            <br /><br />

            <asp:Button
                ID="btnCalculate"
                CssClass="button"
                runat="server"
                Text="Calculate" 
                onclick="btnCalculate_Click"/>

            <asp:Button
                ID="btnClear"
                CssClass="button"
                runat="server"
                Text="Clear" 
                onclick="btnClear_Click"
                CausesValidation="False" />
        </form>
    </section>    
</body>
</html>
